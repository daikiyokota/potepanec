require 'rails_helper'

RSpec.feature "ProductsShow", type: :feature do
  let!(:bags) { create(:taxon, name: "BAGS") }
  let!(:rails) { create(:taxon, name: "RAILS") }
  let!(:product) do
    create(:product, name: "RUBY ON RAILS TOTE",
                     price: 100,
                     description: "this is a test",
                     taxons: [bags, rails])
  end

  before { visit potepan_product_path(product.id) }

  it "/potepan/products/:idのページが表示され、Homeをクリックすると/potepanのページが表示される" do
    expect(page).to have_current_path "/potepan/products/#{product.id}"
    within "#Home" do
      click_link "Home"
    end
    expect(page).to have_current_path '/potepan'
  end

  it "商品名、価格、商品説明が一致している" do
    expect(page).to have_content product.name
    expect(page).to have_content product.price
    expect(page).to have_content product.description
  end
end
