require 'rails_helper'

RSpec.feature "CategoriesShow", type: :feature do
  let!(:categories) { create(:taxonomy, name: 'Categories') }
  let!(:bags) { create(:taxon, name: 'BAGS', taxonomy: categories) }
  let!(:mugs) { create(:taxon, name: 'MAGS', taxonomy: categories) }
  let!(:product) { create(:product, name: 'RUBY ON RAILS TOTE', price: 100, taxons: [bags]) }
  let!(:product2) { create(:product, name: 'RUBY ON RAILS BAG', price: 200, taxons: [bags]) }
  let!(:product3) { create(:product, name: 'RUBY ON RAILS MAG', price: 300, taxons: [mugs]) }

  before { visit potepan_category_path(bags.id) }

  it "/potepan/category/:idのページに飛ぶと、そのカテゴリーの商品一覧が表示されている" do
    expect(page).to have_current_path "/potepan/categories/#{bags.id}"
    expect(page).to have_content product.name, product.display_price
    expect(page).to have_content product2.name, product2.display_price
  end

  it "商品詳細ページで一覧ページへ戻るボタンを押すと、カテゴリーページに戻ることができる" do
    click_link "#{product.name}"
    expect(page).to have_current_path "/potepan/products/#{product.id}"
    click_link '一覧ページへ戻る'
    expect(page).to have_current_path "/potepan/categories/#{bags.id}"
  end

  it "商品カテゴリーの選択欄からカテゴリーを選択すると、そのカテゴリーページが表示される" do
    within first('.panel-body') do
      click_link "#{categories.name}"
      click_link "#{mugs.name}"
    end
    expect(page).to have_current_path "/potepan/categories/#{mugs.id}"
    expect(page).to have_content product3.name, product3.display_price
  end

  it "カテゴリーページでは、そのカテゴリーに属していない商品は表示されない" do
    expect(page).not_to have_content product3.name, product3.display_price
    within first('.panel-body') do
      click_link "#{categories.name}"
      click_link "#{mugs.name}"
    end
    expect(page).not_to have_content product.name, product.display_price
    expect(page).not_to have_content product2.name, product2.display_price
  end
end
