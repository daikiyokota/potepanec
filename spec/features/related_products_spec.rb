require 'rails_helper'

RSpec.feature "RelatedProducts", type: :feature do
  let!(:categories) { create(:taxonomy, name: 'Categories') }
  let!(:bags) { create(:taxon, name: 'BAGS', taxonomy: categories) }
  let!(:rails) { create(:taxon, name: 'RAILS', taxonomy: categories) }
  let!(:mugs) { create(:taxon, name: 'MUGS', taxonomy: categories) }
  let!(:product) { create(:product, name: 'RUBY ON RAILS TOTE', price: 100, taxons: [bags, rails]) }
  let!(:related_product_1) { create(:product, name: "RUBY ON RAILS STEIN", taxons: [mugs, rails]) }
  let!(:related_product_2) { create(:product, name: "RUBY ON RAILS MUG", taxons: [mugs, rails]) }
  let!(:related_product_3) { create(:product, name: "RUBY ON RAILS BAG", taxons: [bags]) }
  let!(:related_product_4) { create(:product, name: "RUBY ON RAILS JERSEY", taxons: [rails]) }
  let!(:non_related_product) { create(:product, name: "PHP MUG", taxons: [mugs]) }

  before { visit potepan_product_path(id: product.id) }

  it "/potepan/products/:idの関連商品の欄では、その商品と同じカテゴリーの商品が表示されている" do
    within '.productsContent' do
      expect(page).to have_content related_product_1.name, related_product_1.display_price
      expect(page).to have_content related_product_2.name, related_product_2.display_price
      expect(page).to have_content related_product_3.name, related_product_3.display_price
      expect(page).to have_content related_product_4.name, related_product_4.display_price
    end
  end

  it "/potepan/products/:idの関連商品の欄では、その商品と別のカテゴリーの商品は表示されない" do
    expect(page).not_to have_content non_related_product.name, non_related_product.display_price
  end

  it "関連商品をクリックすると、商品ページに飛ぶことができる" do
    click_link related_product_1.name
    expect(page).to have_current_path "/potepan/products/#{related_product_1.id}"
    within '.media-body' do
      expect(page).to have_content related_product_1.name, related_product_1.display_price
    end
  end
end
