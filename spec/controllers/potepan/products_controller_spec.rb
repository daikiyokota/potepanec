require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, name: "RUBY ON RAILS TOTE", taxons: [taxon]) }
    let!(:related_product_1) { create(:product, name: "RUBY ON RAILS STEIN", taxons: [taxon]) }
    let!(:related_product_2) { create(:product, name: "RUBY ON RAILS MUG", taxons: [taxon]) }
    let!(:related_product_3) { create(:product, name: "RUBY ON RAILS BAG", taxons: [taxon]) }
    let!(:related_product_4) { create(:product, name: "RUBY ON RAILS JERSEY", taxons: [taxon]) }
    let!(:non_related_product) { create(:product, name: "PHP MUG", taxons: [other_taxon]) }

    before { get :show, params: { id: product.id } }

    context "when there are 4 related_products" do
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "assigns @product" do
        expect(assigns(:product)).to eq product
      end

      it "includes related_products" do
        expect(assigns(:related_products)).to contain_exactly(related_product_1, related_product_2,
                                                              related_product_3, related_product_4)
      end

      it "does not include non_related_product" do
        expect(assigns(:related_products)).not_to contain_exactly(non_related_product)
      end

      it "renders the :show template" do
        expect(response).to render_template :show
      end
    end

    context "when there are 5 related_products" do
      let!(:related_product_5) { create(:product, name: "RUBY ON RAILS T-SHIRT", taxons: [taxon]) }

      it "includes 4 related_products in @related_products" do
        expect(assigns(:related_products).size).to eq 4
      end
    end
  end
end
