require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product1) { create(:product, name: "RUBY ON RAILS TOTE", taxons: [taxon]) }
    let(:product2) { create(:product, name: "RUBY ON RAILS MUG", taxons: [taxon]) }

    before { get :show, params: { id: taxon.id } }

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "assigns @category" do
      expect(assigns(:category)).to eq taxon
    end

    it "assigns @products" do
      expect(assigns(:products)).to eq [product1, product2]
    end

    it "assigns @taxonomies" do
      expect(assigns(:taxonomies)).to eq Spree::Taxonomy.all
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
