class Potepan::CategoriesController < ApplicationController
  def show
    @category = Spree::Taxon.find(params[:id])
    @products = @category.all_products.includes(master: [:default_price, :images])
    @taxonomies = Spree::Taxonomy.all.includes(taxons: :products)
  end
end
