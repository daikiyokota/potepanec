class Potepan::ProductsController < ApplicationController
  NUMBER_OF_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.in_taxons(@product.taxons).distinct.
      where.not(id: @product.id).includes(master: [:default_price, :images]).
      sample(NUMBER_OF_RELATED_PRODUCTS)
  end
end
